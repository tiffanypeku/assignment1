class Crawler
  class Page
    include Celluloid
    def initialize(url,visited_urls)
      @url = url
      @visited_urls = visited_urls
      agent = Mechanize.new
      agent.get @url
      @content = agent.get @url
    rescue Exception => e
      puts "failed to reach the destination: #{e.to_s}"
    end

    def process_page
      return if @visited_urls.keys.include?(@url.to_sym)
      links = extract_urls
      input_counter = pattern_counter("input")
      md =  Metadata.new(links,input_counter)
      @visited_urls[@url.to_sym] = md
      md
    end

    def metadata(process_page_future)
      return 0 if process_page_future.value.nil?
      process_page_future.value
    end

    def extract_urls
      links = []
      @content.links.each do |link|
        next if link.uri.to_s == "/"
        abs = to_absolute(link)
        links << abs if (in_domain?(abs))
      end
      links.uniq
    end

    def pattern_counter(css_pattern)
      @content.css(css_pattern).count
    end

    def get_counter(pattern_counter_future)
      pattern_counter_future.value
    end

    def to_absolute(link)
      @content.uri.merge(link.uri).to_s.delete('#')
    end

    def in_domain?(url)
      URI(url).host.eql? @content.uri.host
    end
  end
end
