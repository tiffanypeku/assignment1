class Crawler
  class Metadata
    attr_reader :internal_links, :input_counter
    def initialize(internal_links=[],input_counter=0)
      @internal_links = internal_links
      @input_counter = input_counter
    end
  end
end
