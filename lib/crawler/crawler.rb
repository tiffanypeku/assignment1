class Crawler
  def initialize(url, depth = 1, page_max = 20)
    @orig_url     = url
    @visited_urls = {}
    @url_queues   = []
    @depth        = depth
    @page_max     = page_max
  end

  def crawl
    init_limit_checker_variables
    @url_queues.push @orig_url
    while(!@url_queues.empty?)
      current_url = @url_queues.pop.to_sym
      process Page.new(current_url, @visited_urls)
      input_counter = counter(current_url)
      if time_to_depth_increase?(children_urls(current_url).size)
        inc_depth_variables
        break if limit_reached?
        @elements_to_depth_increase = @next_elements_to_depth_increase
        @next_elements_to_depth_increase = 0
      end
      children_urls(current_url).each do |url|
        break if limit_reached?
        child_page = Page.new(url,@visited_urls)
        process_page_future = child_page.future :process_page
        child_page.metadata process_page_future
        input_counter = input_counter+counter(url)
        @url_queues.push url
      end
    end
    output_result
  end

  def limit_reached?
    @current_depth > @depth or @visited_urls.keys.count+1 > @page_max
  end

  private

  def init_limit_checker_variables
    @current_depth = 0
    @elements_to_depth_increase = 1
    @next_elements_to_depth_increase = 0
  end

  def output_result
    @visited_urls.each do |url, metadata|
      puts "#{url} - #{metadata.input_counter} inputs"
    end
  end

  def process(page)
    page.process_page
  end

  def time_to_depth_increase?(children_urls_size)
    @next_elements_to_depth_increase = @next_elements_to_depth_increase+children_urls_size
    @elements_to_depth_increase -= 1
    @elements_to_depth_increase.eql? 0
  end

  def inc_depth_variables
    @elements_to_depth_increase += 1
    @current_depth += 1
  end

  def counter(url)
    @visited_urls[url.to_sym].input_counter
  end

  def children_urls(url)
    @visited_urls[url].internal_links
  end
end
