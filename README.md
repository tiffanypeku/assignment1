# Crawler

[![Code Climate](https://codeclimate.com/github/aligit/s2mtest/badges/gpa.svg)](https://codeclimate.com/github/aligit/s2mtest)
[![Coverage Status](https://coveralls.io/repos/aligit/s2mtest/badge.svg?branch=master&service=github)](https://coveralls.io/github/aligit/s2mtest?branch=master)
[![Build Status](https://travis-ci.org/aligit/s2mtest.svg?branch=master)](https://travis-ci.org/aligit/s2mtest)

run `bin/console` for an interactive prompt.

## Installation

Clone the source code and get into it:

```bash
git clone https://tiffanypeku@bitbucket.org/tiffanypeku/s2m.git
cd s2m.git
```

Then from within the code, install the gem

```bash
gem build crawler.gemspec
```

Finally install it by:

```bash
gem install crawler-0.1.0.gem
```

## Algorithm
BFS has been used for crawling within specified depth. For concurrency,  
an actor-based approach has been chosen. To that end, I used celluloid  
gem.

## Usage

There are two ways to use this gem. If you installed the gem following  
the previous, then you can simply require it in your code like so:


```ruby
require 'crawler'
```

For a simpler usage use the following command to run the script.

```bash
./bin/console help crawl
```

example

```bash
./bin/console crawl http://kijkshop.demo.shop2market.com --depth=1 --max-pages=2
```

## Development

After checking out the repo, run `bin/setup` to install dependencies.  
Then, run `rake spec` to run the tests. You can also run `bin/console`  
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake  
install`. To release a new version.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
