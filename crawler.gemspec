# coding: utf-8
$:.push File.expand_path("../lib", __FILE__)
require 'crawler/version'

Gem::Specification.new do |spec|
  spec.name          = "crawler"
  spec.version       = Crawler::VERSION
  spec.authors       = ["ali fakheri"]
  spec.email         = ["fakheri.ali@gmail.com"]
  spec.summary       = "crawles Website considering performance"
  spec.description   = "no desc"
  spec.homepage      = "https://bitbucket.org/tiffanypeku/s2m"
  spec.license       = "MIT"
  spec.files         = `git ls-files`.split("\n")
  spec.test_files    = `git ls-files -- {spec}/*`.split("\n")
  spec.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "nokogiri", "~> 1.6.7.1"
  spec.add_dependency "mechanize"
  spec.add_dependency "celluloid"
  spec.add_dependency "thor"
  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
end
