require 'spec_helper'

describe Crawler do
  let(:url) { 'http://google.com' }
  let(:url_real) { 'http://kijkshop.demo.shop2market.com' }
  # let(:url_real) { 'http://localhost:3000/home/don' }
  let(:crawler_real) { Crawler.new(url_real) }
  describe '.new' do
    it 'instantiate new crawler' do
      expect { Crawler.new(url) }.to_not raise_error
    end
  end

  describe '#crawl' do
    it 'returns result of crawl' do
      expect(crawler_real.crawl).to_not be_nil
    end
  end

  describe '#limit_reached' do
    let(:depth) { 2 }
    let(:max_page) { 2 }
    let(:limited_crawler) { Crawler.new(url_real,depth,max_page) }
    context 'if either level depth or maximum page numbers are reached' do
      let(:current_depth) { 2 }
      let(:visited_urls) {{
        "url1": ["url11","url12"],
        "url2": ["url21","url22"],
        "url3": ["url31","url32"],
      }}
      it 'returns true' do
        limited_crawler.instance_variable_set(:@visited_urls,visited_urls)
        limited_crawler.instance_variable_set(:@current_depth,current_depth)
        expect(limited_crawler.limit_reached?).to be_truthy
      end
    end
  end
end
