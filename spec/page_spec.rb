require 'spec_helper'

class Crawler
  describe Page do
    let(:url) { 'http://kijkshop.demo.shop2market.com' }
    let(:agent) { Mechanize.new }
    let(:page) { agent.get url }
    let(:visited_urls) {{
      "url1": ["url11","url12"],
      "url2": ["url21","url22"],
      "url3": ["url31","url32"],
    }}
    subject { Page.new(url, visited_urls) }
    describe '.new' do
      it 'instantiate new page' do
        expect { Page.new(url, visited_urls) }.to_not raise_error
      end
      it 'should raise an exception if the page is unreachable or network error' do
        expect {
          Page.new(url, visited_urls)
          raise
        }.to raise_error(Exception)
      end
    end

    describe '#extract_urls' do
      it 'returns urls of a page' do
        expect(subject.extract_urls).to be_a Array
      end
    end

    describe '#pattern_counter' do
      let(:pattern) { "input" }
      it 'returns number of patterns within the current page' do
        expect(subject.pattern_counter(pattern)).to be >= 0
      end
    end

    describe '#to_absolute' do
      let(:relative_link) { page.links.select {|s| s.href.to_s.start_with?("/")}.last }
      it 'given a url, returns the absolute url' do
        expect(subject.to_absolute(relative_link))
          .to eq page.uri.merge(relative_link.uri).to_s
      end
    end

    describe '#in_domain' do
      let(:internal_uri) { URI(url+'/orders') }
      let(:external_uri) { URI("http://google.com") }
      context 'if given uri does not belong to domain' do
        it 'return false' do
          expect(subject.in_domain?(external_uri)).to be_falsy
        end
      end

      context 'if given uri does belong to domain' do
        it 'returns true' do
          expect(subject.in_domain?(internal_uri)).to be_truthy
        end
      end
    end
  end
end
